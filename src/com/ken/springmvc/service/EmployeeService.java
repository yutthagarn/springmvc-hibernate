package com.ken.springmvc.service;

import java.util.List;

import com.ken.springmvc.model.Employee;

public interface EmployeeService {

	public void saveEmployee(Employee employee);
	public List<Employee> findAllEmployee();
	public void deleteEmployeeBySsn(String ssn);
	
}
