package com.ken.springmvc.dao;

import java.util.List;

import com.ken.springmvc.model.Employee;

public interface EmployeeDao {

	public void saveEmployee(Employee employee);
	public List<Employee> findAllEmployee();
	public void deleteEmployeeBySsn(String ssn);
	
}
