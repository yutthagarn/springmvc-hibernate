package com.ken.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.ken.springmvc.model.Employee;

@Repository("employeeDao")
public class EmployeeDaoImpl extends AbstractDao implements EmployeeDao {

	@Override
	public void saveEmployee(Employee employee) {
		persist(employee);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> findAllEmployee() {
		Criteria criteria = getSession().createCriteria(Employee.class);
		return (List<Employee>)criteria.list();
	}

	@Override
	public void deleteEmployeeBySsn(String ssn) {
		Query query = getSession().
		createSQLQuery("delete from Employee where ssn = :ssn");
		query.setString("ssn", ssn);
		query.executeUpdate();
	}

}
