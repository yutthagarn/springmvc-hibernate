package com.ken.springmvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ken.springmvc.model.Employee;
import com.ken.springmvc.service.EmployeeService;

@Controller
@RequestMapping("/")
public class AppController {

	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping(value = {"/","/list"}, method = RequestMethod.GET)
	public String listEmployyees(ModelMap model){
		
		List<Employee> employees = employeeService.findAllEmployee();
		model.addAttribute("employees",employees);
		return "allemployees";
		
	}
	
	@RequestMapping(value = {"/new"}, method = RequestMethod.GET)
	public String newEmployee(ModelMap model){
		Employee employee = new Employee();
		model.addAttribute("employee",employee);
		return "registration";
	}
	
	@RequestMapping(value = {"/new"}, method = RequestMethod.POST)
	public String saveEmployee(@Valid Employee employee, BindingResult result,
			ModelMap model){
		
		if(result.hasErrors()){
			return "registration";
		}
		
		employeeService.saveEmployee(employee);
		
		model.addAttribute("success", "Employee "+employee.getName()
				+" registered successfully");
		
		return "success";
		
	}
	
	@RequestMapping(value = {"/delete-{ssn}-employee"}, method = RequestMethod.GET)
	public String deleteEmployee(@PathVariable String ssn){
		employeeService.deleteEmployeeBySsn(ssn);
		return "redirect:/list";
	}
	
}
