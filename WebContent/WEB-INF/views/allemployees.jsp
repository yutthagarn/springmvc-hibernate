<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>:: All Employee ::</title>
<style type="text/css">
tr:first-child {
	font-weight: bold;
	background-color: #C6C9C4;
}
</style>
</head>
<body>
	<h2>List of Employees</h2>
	<table>
		<tr>
			<td>NAME</td>
			<td>Joining Date</td>
			<td>Salary</td>
			<td>SSN</td>
			<td>Delete</td>
		</tr>
		<c:forEach items="${employees }" var="emp">
			<tr>
				<td>${emp.name }</td>
				<td>${emp.joiningDate }</td>
				<td>${emp.salary }</td>
				<td>${emp.ssn }</td>
				<td><a href="<c:url value='/delete-${emp.ssn }-employee' />">Delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<br>
	<a href="<c:url value='/new' />">Add New Employee</a>
</body>
</html>